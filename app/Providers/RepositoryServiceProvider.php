<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Eloquent\EloquentTrackingCodeRepository;
use App\Repositories\Contracts\TrackingCodeRepository;
use App\Repositories\CSV\CSVTrackingCodeRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //here you can switch/replace storage solutions
        // $this->app->bind(TrackingCodeRepository::class, EloquentTrackingCodeRepository::class);
        $this->app->bind(TrackingCodeRepository::class, CSVTrackingCodeRepository::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
