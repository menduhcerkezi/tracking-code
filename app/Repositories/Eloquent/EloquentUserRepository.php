<?php 

namespace App\Repositories\Eloquent;

use App\User;
use App\Repositories\EloquentRapositoryAbstract;
use App\Repositories\Contracts\UserRepository;

class EloquentUserRepository extends EloquentRapositoryAbstract implements UserRepository
{
	public function entity()
	{
		return User::class;
	}

	public function createAddress($userId, array $properties)
	{
		return $this->find($userId)->address()->create($properties);
	}

	public function deleteAddress($userId, $addressId)
	{
		return $this->find($userId)->address()->findOrFail($addressId)->delete();
	}
}