<?php 

namespace App\Repositories\Eloquent;

use App\TrackingCode;
use App\Repositories\EloquentRapositoryAbstract;
use App\Repositories\Contracts\TrackingCodeRepository;


class EloquentTrackingCodeRepository extends EloquentRapositoryAbstract implements TrackingCodeRepository
{
	public function entity()
	{
		return TrackingCode::class;
	}
}