<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Criteria\CriteriaInterface;
use App\Repositories\Exceptions\NoEntityDefined;

abstract class EloquentRapositoryAbstract implements RepositoryInterface, CriteriaInterface
{
	protected $entity;

	public function __construct()
	{
		$this->entity = $this->resolveEntity();
	}

	public function findWhereCode($value)
	{
		$code = $this->entity->where('code', $value)->first();

		return ['code' => $code->code, 'estimated_at' => $code->estimated_at];
	}

	public function withCriteria(...$criteria)
	{
		$criteria = array_flatten($criteria);

		foreach ($criteria as $criterion) {
			$this->entity = $criterion->apply($this->entity);
		}

		return $this;
	}

	protected function resolveEntity()
	{
		if(!method_exists($this, 'entity')) {
			throw new NoEntityDefined("No entity defined", 1);			
		}

		return app()->make($this->entity());
	}
}