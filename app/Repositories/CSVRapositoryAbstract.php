<?php 
namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Criteria\CriteriaInterface;
use App\Repositories\Exceptions\NoEntityDefined;

abstract class CSVRapositoryAbstract implements RepositoryInterface
{
    protected $storageFile;

    public function __construct()
    {
        $this->storageFile = storage_path('app\\'. env('tracking-codes'));
        //there was an issue with my env; I had to work with hardcoded
        // $this->storageFile = "C:\\xampp\htdocs\\tracking-code\storage\app\\tracking-codes.csv";
    }
    
    public function findWhereCode($value)
    {
        //a simple exmple wich works well with small CSV files
        $ch = fopen($this->storageFile, "r");
        $header_row = fgetcsv($ch);

        /* This will loop through all the rows until it reaches the end */
        while ($row = fgetcsv($ch)) {
            //in CSV file we have two columns code ($row[0]) and estimated_at ($row[1])
            if($row[0] == $value) {
                return ['code' => $row[0], 'estimated_at' => $row[1]];
            }
        }
    }
}