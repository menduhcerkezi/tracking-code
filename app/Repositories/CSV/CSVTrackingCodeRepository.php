<?php 

namespace App\Repositories\CSV;

use App\Repositories\CSVRapositoryAbstract;
use App\Repositories\Contracts\TrackingCodeRepository;

class CSVTrackingCodeRepository extends CSVRapositoryAbstract implements TrackingCodeRepository
{
}