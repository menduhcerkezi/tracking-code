<?php

namespace App\Repositories\Contracts;

interface RepositoryInterface 
{
	public function findWhereCode($value);
}