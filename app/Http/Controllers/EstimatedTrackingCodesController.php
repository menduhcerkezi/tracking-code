<?php

namespace App\Http\Controllers;
use App\Repositories\Contracts\TrackingCodeRepository;

use Illuminate\Http\Request;

class EstimatedTrackingCodesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TrackingCodeRepository $trackingCodes, $trackingCode)
    {
        //As you can see here; with implementation of repositories we do not change on replacing storage solutions
        return response()->json($trackingCodes->findWhereCode($trackingCode));
    }

}
