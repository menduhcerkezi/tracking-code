<?php

namespace Tests\Feature;

use App\TrackingCode;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EloquentEstimatedTrackingCodeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testItCanFindATrackingCode()
    {
        $trackingCode = factory(TrackingCode::class)->create();

        $this->get("api/estimated-tracking-code/{$trackingCode->code}")
            ->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testItReturnsAValidJson()
    {
        $trackingCode = factory(TrackingCode::class)->create();

        $this->get(route("find.code", ['id' => $trackingCode->code]))
            ->assertJson([
                'code' => $trackingCode->code,
                'estimated_at' => $trackingCode->estimated_at
            ]);
    }    
}
