<?php

use Faker\Generator as Faker;

$factory->define(App\TrackingCode::class, function (Faker $faker) {
    return [
        'code' => $faker->randomNumber(),
        'estimated_at' => $faker->dateTimeThisMonth()->format('Y-m-d H:i:s')
    ];
});
